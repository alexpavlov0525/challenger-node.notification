import { WorkerProvider } from './workerProvider';
import { ServerCommunicationHandler } from './serverCommunicationHandler';
import { ServersProvider } from './serverProvider';
import { Configuration } from './../configuration';
import { ClientProvider } from './clientProvider';
import { WorkerType } from '../enums/worker-type';
import * as jwt from 'jsonwebtoken';
import * as express from 'express';


export class NotificationHandler {
    clientProvider: ClientProvider;
    serversProvider: ServersProvider;
    workerProvider: WorkerProvider;
    io: any;
    serverCommunicationHandler: ServerCommunicationHandler;

    constructor(io: any) {
        this.io = io;
        this.clientProvider = new ClientProvider();
        this.serversProvider = new ServersProvider();
        this.workerProvider = new WorkerProvider();
        this.serverCommunicationHandler = new ServerCommunicationHandler(this.io, this.clientProvider, this.workerProvider);
    }

    async handleConnection(client: any) {
            let clientName = client.handshake.query.username;

            if (client.handshake.query.token === null) {
                if (this.clientProvider.get(clientName)) {
                    this.clientProvider.delete(clientName);
                }
                console.log(`client ${clientName} was missing token. disconnected`);
                client.disconnect();
            } else {
                let isValidToken = true;
                let result: any;
                try {
                    result = await jwt.verify(client.handshake.query.token, Configuration.secret);
                    console.log('result', result);
                } catch (error) {
                    console.log('Error occured:', error.message);
                    isValidToken = false;
                    client.disconnect();
                }

                if (isValidToken) {
                    let serverName = result['serverId'];
                    let workerName = result['workerId'];
                    if (serverName) {
                        this.serversProvider.set(serverName, client.id);
                        console.log('Server connected...', serverName);

                        this.serverCommunicationHandler.handleStartGame(client, serverName);

                        this.serverCommunicationHandler.handleJoinedGame(client, serverName);

                        this.serverCommunicationHandler.handleStartRound(client, serverName);

                        this.serverCommunicationHandler.handleStartedRound(client, serverName);

                        this.serverCommunicationHandler.handleFinishRound(client, serverName);

                        this.serverCommunicationHandler.handleFinishedRound(client, serverName);

                        this.serverCommunicationHandler.handleFinishedGame(client, WorkerType.StatsWorker);

                        client.on('disconnect', () => {
                            console.log(`server ${serverName} disconnected...`);
                            this.serversProvider.delete(result['serverId']);

                        });
                    } else if (workerName) {
                        this.workerProvider.set(workerName, client.id);
                        console.log('Worker connected...', workerName);

                        client.on('disconnect', () => {
                            console.log(`worker ${workerName} disconnected...`);
                            this.workerProvider.delete(result['workerId']);
                        });
                    } else {
                        console.log('Client connected...', clientName);
                        this.clientProvider.set(clientName, client.id);

                        client.on('disconnect', () => {
                            console.log(`client ${clientName} disconnected...`);
                            this.clientProvider.delete(clientName);
                        });
                    }

                    console.log('clients: ', this.clientProvider.getAll());
                    console.log('servers: ', this.serversProvider.getAll());
                    console.log('workers: ', this.workerProvider.getAll());
                }
            }
    }

    handleStart(request: express.Request, response: express.Response, io: any) {
        io.emit('hello', 'hi there');
        response.send({success: true});
    }
}