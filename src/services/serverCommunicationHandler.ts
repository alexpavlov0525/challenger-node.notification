import { WorkerProvider } from './workerProvider';
import { ClientProvider } from './clientProvider';
import * as socket from 'socket.io';

export class ServerCommunicationHandler {
    io: any
    clientProvider: ClientProvider;
    workerProvider: WorkerProvider;
    constructor(io: any, clientProvider: ClientProvider, workerProvider: WorkerProvider) {
        this.io = io;
        this.clientProvider = clientProvider;
        this.workerProvider = workerProvider;
    }

    async handleStartGame(client: any, serverName: string) {
        client.on('startgame', (payload: any) => {
            console.log(`[startgame] recieved from server::${serverName}`);
            if (payload.recipients) {
                payload.recipients.forEach((user: any) => {
                    client.broadcast.to(this.clientProvider.get(user)).emit('message', payload);
                });
            }
        });
    }

    async handleJoinedGame(client: any, serverName: string) {
        client.on('joinedgame', (payload: any) => {
            console.log(`[joinedgame] recieved from server::${serverName}`);
            if (payload.recipients) {
                payload.recipients.forEach((user: any) => {
                    client.broadcast.to(this.clientProvider.get(user)).emit('refresh', payload.message);
                });
            }
        });
    }

    async handleStartRound(client: any, serverName: string) {
        client.on('startround', (payload: any) => {
            console.log(`[startround] recieved from server::${serverName}`);
            if (payload.recipients) {
                payload.recipients.forEach((user: any) => {
                    client.broadcast.to(this.clientProvider.get(user)).emit('message', payload);
                });
            }
        });
    }

    async handleStartedRound(client: any, serverName: string) {
        client.on('startedround', (payload: any) => {
            console.log(`[startedround] recieved from server::${serverName}`);
            if (payload.recipients) {
                payload.recipients.forEach((user: any) => {
                    client.broadcast.to(this.clientProvider.get(user)).emit('refresh', payload.message);
                });
            }
        });
    }

    async handleFinishRound(client: any, serverName: string) {
        client.on('finishround', (payload: any) => {
            console.log(`[finishround] recieved from server::${serverName}`);
            if (payload.recipients) {
                payload.recipients.forEach((user: any) => {
                    client.broadcast.to(this.clientProvider.get(user)).emit('message', payload);
                });
            }
        });
    }    

    async handleFinishedRound(client: any, serverName: string) {
        client.on('finishedround', (payload: any) => {
            console.log(`[finishedround] recieved from server::${serverName}`);
            if (payload.recipients) {
                payload.recipients.forEach((user: any) => {
                    client.broadcast.to(this.clientProvider.get(user)).emit('refresh', payload.message);
                });
            }
        });
    }

    async handleFinishedGame(client: any, workerName: string) {
        client.on('finishedgame', (payload: any) => {
            console.log(`[finishedgame] recieved. hitting worker::${workerName}`);
            if (payload.recipients) {
                client.broadcast.to(this.workerProvider.get(workerName)).emit('scan', payload.recipients);
            }
        });
    }  

}