export class Provider {
    private objects: { [name: string]: string } = {};

    set(key: string, value: string) {
        this.objects[key] = value;
    }

    get(key: string): string {
        return this.objects[key];
    }

    delete(key: string) {
        delete this.objects[key];
    } 

    getAll() {
        return this.objects;
    }
}