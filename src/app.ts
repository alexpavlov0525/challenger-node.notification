import { NotificationHandler } from './services/notificationHandler';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as socket from 'socket.io';
import * as jwt from 'jsonwebtoken';
import * as cors from './middleware/cors';
import * as authHandler from './middleware/authentication';
import { Configuration } from './configuration';

// import * as authHandler from './middleware/authentication';
// import * as cors from './middleware/cors';
import * as requestLogger from './middleware/requestLogger';

export class WebApi {
    io: any;
    notificationHandler: NotificationHandler;
    clients: { [name: string]: string } = {};
    servers: { [name: string]: string } = {};

    /**
     * @param app - express application
     * @param port - port to listen on
     */
    constructor(private app: express.Express, private port: number) {
        this.configureMiddleware(app);
        this.configureRoutes(app);
        this.notificationHandler = new NotificationHandler(this.io);
        //   this.configureSockets();
    }

    private configureSockets(app: any) {
        this.io = socket(app);

        this.io.on('connection', async (client: any) => {
            await this.notificationHandler.handleConnection(client);
        });
    }

    /**
     * @param app - express application
     */
    private configureMiddleware(app: express.Express) {
        app.use(requestLogger);
        app.use(cors);
        app.use('/notification', authHandler);
    }

    private configureRoutes(app: express.Express) {
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(bodyParser.json());
        // app.use('/notification/startgame', async (req, res) => 
        //     this.notificationHandler.handleStart(req, res, this.io));
    }

    public run() {
        let server = this.app.listen(this.port);
        this.configureSockets(server);
    }
}