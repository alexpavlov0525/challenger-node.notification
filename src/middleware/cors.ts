import * as express from 'express';

let cors: express.RequestHandler = (
    request: express.Request,
    response: express.Response,
    next: express.NextFunction
) => {
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    response.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    if (request.method === 'OPTIONS') {
      response.statusCode = 200;
      response.send();
    } else {
      next();
    }
}

export = cors;