import { Configuration } from './configuration';
import * as express from 'express';
import { WebApi } from './app';

let port = Configuration.api_host_port;

let api = new WebApi(express(), port);
api.run();
console.info(`listening on ${port}`);